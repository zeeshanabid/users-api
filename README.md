# users-api

A simple REST api and client to demonstrate users registration and authentication in golang.

## Requirements

* docker
* docker-compose
* golang 1.9 (for client)

## Usage

Run api in docker container.
`docker-compose up api`

Run tests in docker container.
`docker-compose up test`

## Client

A command line client is included.

Build the command line client. 
`go build -o user ./client`

Create user.
`./user create --username=<username> --password=<password>`

Login.
`./user login --username=<username> --password=<password>`

List Sessions.
Create user.
`./user list-sessions`
