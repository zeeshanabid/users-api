package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

var apiURL = getAPIUrl()
var sessionFile = ".session"

func getAPIUrl() string {
	apiUrl := os.Getenv("API")
	if apiUrl == "" {
		apiUrl = "http://localhost:8080"
	}
	return apiUrl
}

func decodeHttpError(body io.ReadCloser) error {
	var httpError HttpError
	decoder := json.NewDecoder(body)
	err := decoder.Decode(&httpError)
	if err != nil {
		return err
	}
	return errors.New(httpError.Msg)
}

func createUser(username, password string) error {
	data := CreateUserRequest{
		Username: username,
		Password: password,
	}

	body, err := json.Marshal(data)
	if err != nil {
		return err
	}

	r, err := http.NewRequest("POST", fmt.Sprintf("%s/user", apiURL), bytes.NewBuffer(body))
	if err != nil {
		return err
	}

	r.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(r)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusCreated {
		return nil
	} else {
		return decodeHttpError(resp.Body)
	}
}

func loginUser(username, password string) (*UserSession, error) {
	data := LoginRequest{
		Username: username,
		Password: password,
	}

	body, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}

	r, err := http.NewRequest("POST", fmt.Sprintf("%s/user/login", apiURL), bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	r.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(r)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		var session UserSession
		decoder := json.NewDecoder(resp.Body)
		err = decoder.Decode(&session)
		if err != nil {
			return nil, err
		}
		return &session, nil
	} else {
		return nil, decodeHttpError(resp.Body)
	}
}

func listSessions(token string) ([]UserSessionResponse, error) {
	r, err := http.NewRequest("GET", fmt.Sprintf("%s/user/session", apiURL), nil)
	if err != nil {
		return nil, err
	}

	r.Header.Set("Authorization", token)
	resp, err := http.DefaultClient.Do(r)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		var sessions []UserSessionResponse
		decoder := json.NewDecoder(resp.Body)
		err = decoder.Decode(&sessions)
		if err != nil {
			return nil, err
		}
		return sessions, nil
	} else {
		return nil, decodeHttpError(resp.Body)
	}
}

func main() {
	createCommand := flag.NewFlagSet("create", flag.ExitOnError)

	createUsername := createCommand.String("username", "", "Username to register (Required)")
	createPassword := createCommand.String("password", "", "Password of the login (Required)")

	loginCommand := flag.NewFlagSet("login", flag.ExitOnError)
	loginUsername := loginCommand.String("username", "", "Username to login (Required)")
	loginPassword := loginCommand.String("password", "", "Password of the login (Required)")

	listSessionCommand := flag.NewFlagSet("list-sessions", flag.ExitOnError)

	if len(os.Args) < 2 {
		fmt.Println("Supported subcommands are create, login and list-sessions")
		os.Exit(1)
	}

	switch os.Args[1] {
	case "create":
		createCommand.Parse(os.Args[2:])
	case "login":
		loginCommand.Parse(os.Args[2:])
	case "list-sessions":
		listSessionCommand.Parse(os.Args[2:])
	default:
		fmt.Println("Supported subcommands are create, login and list-sessions")
		os.Exit(1)
	}

	if createCommand.Parsed() {
		if *createUsername == "" {
			createCommand.PrintDefaults()
			os.Exit(1)
		}

		if *createPassword == "" {
			createCommand.PrintDefaults()
			os.Exit(1)
		}

		err := createUser(*createUsername, *createPassword)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println("User created")
		return
	}

	if loginCommand.Parsed() {
		if *loginUsername == "" {
			loginCommand.PrintDefaults()
			os.Exit(1)
		}

		if *loginPassword == "" {
			loginCommand.PrintDefaults()
			os.Exit(1)
		}

		session, err := loginUser(*loginUsername, *loginPassword)
		if err != nil {
			fmt.Println(err)
			return
		}
		err = ioutil.WriteFile(sessionFile, []byte(session.Token), 0644)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println(fmt.Sprintf("Session created in %s file", sessionFile))
		return
	}

	if listSessionCommand.Parsed() {
		data, err := ioutil.ReadFile(sessionFile)
		if err != nil {
			fmt.Println("Cannot read session token")
			return
		}
		sessions, err := listSessions(string(data))
		if err != nil {
			fmt.Println(err)
			return
		}
		for _, session := range sessions {
			fmt.Println(fmt.Sprintf("Token=%s From=%s, To=%s",
				session.Token,
				session.ValidFrom.Format(time.RFC822),
				session.ValidTill.Format(time.RFC822),
			))
		}
		return
	}
}
