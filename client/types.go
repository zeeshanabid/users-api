package main

import "time"

type UserSessionResponse struct {
	Username  string    `json:"username"`
	Token     string    `json:"token"`
	ValidFrom time.Time `json:"valid_from"`
	ValidTill time.Time `json:"valid_till"`
}

type CreateUserRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type LoginRequest = CreateUserRequest

type UserSession struct {
	Username string `json:"username"`
	Token    string `json:"token"`
}

type HttpError struct {
	Status string `json:"status"`
	Msg    string `json:"message"`
}
