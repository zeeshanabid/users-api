FROM golang:alpine

WORKDIR /go/src/users
COPY . /go/src/users
RUN apk update && \
    apk add glide && \
    apk add git && \
    glide install

RUN go build -o users-api ./api
CMD ["./users-api"]
