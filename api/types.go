package main

import "time"

type DBUser struct {
	ID        int64      `db:"id"`
	Username  string     `db:"username"`
	Password  string     `db:"password"`
	CreatedAt *time.Time `db:"created_at"`
}

type DBUserSession struct {
	ID        int64     `db:"id" json:"-"`
	Username  string    `db:"username" json:"username"`
	Token     string    `db:"token" json:"token"`
	ValidFrom time.Time `db:"valid_from" json:"valid_from"`
	ValidTill time.Time `db:"valid_till" json:"valid_till"`
}

type CreateUserRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type LoginRequest = CreateUserRequest

type UserSession struct {
	Username string `json:"username"`
	Token    string `json:"token"`
}

type HttpError struct {
	Status string `json:"status"`
	Msg    string `json:"message"`
}
