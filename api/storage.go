package main

import (
	"database/sql"
	"log"
	"os"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
)

var schema = []string{
	`CREATE TABLE IF NOT EXISTS users (
		id         SERIAL,
		username   TEXT,
		password   TEXT,
		created_at TIMESTAMP DEFAULT now(),

		CONSTRAINT pk_users PRIMARY KEY(id),
		CONSTRAINT uniq_username UNIQUE(username)
	)`,
	`CREATE TABLE IF NOT EXISTS user_sessions (
		id         SERIAL,
		username   TEXT,
		token	   uuid,
		valid_from TIMESTAMP,
		valid_till TIMESTAMP,

		CONSTRAINT pk_user_sessions PRIMARY KEY(id),
		CONSTRAINT fk_user_sessions_username FOREIGN KEY (username) REFERENCES users(username)
	)`,
	`CREATE INDEX IF NOT EXISTS ix_user_sessions_username
		ON user_sessions(username)
	`,
}

const sessionDuration = 20 * time.Minute

type UsersStorage struct {
	DB *sqlx.DB
}

func (us *UsersStorage) initSchema() error {
	for _, q := range schema {
		_, err := us.DB.Exec(q)
		if err != nil {
			return err
		}
	}
	return nil
}

func (us *UsersStorage) CreateUser(username, password string) error {
	user := &DBUser{
		Username: username,
		Password: password,
	}
	q := `INSERT INTO users (username, password)
			VALUES (:username, :password)`

	_, err := us.DB.NamedExec(q, user)
	if err != nil {
		if isUniqueError(err) {
			return UserAlreadyExistsError
		}
		return err
	}
	return nil
}

func (us *UsersStorage) AuthenticateUser(username, password string) (*DBUser, error) {
	user := &DBUser{
		Username: username,
		Password: password,
	}
	q := `SELECT id, username, password, created_at
			FROM users
			WHERE username = :username AND password = :password`

	result, err := us.DB.NamedQuery(q, user)
	if err != nil {
		return nil, err
	}
	defer result.Close()

	if result.Next() {
		err = result.StructScan(user)
		if err != nil {
			return nil, err
		}
		return user, nil
	}
	return nil, UserNotFound
}

func (us *UsersStorage) ListSessions(username string) ([]DBUserSession, error) {
	q := `SELECT id, username, token, valid_from, valid_till
			FROM user_sessions
			WHERE username = $1 AND valid_from <= $2 AND valid_till >= $2
			ORDER BY valid_from DESC
			LIMIT 5`

	now := time.Now().UTC()

	rows, err := us.DB.Queryx(q, username, now)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var sessions []DBUserSession
	for rows.Next() {
		var session DBUserSession
		err = rows.StructScan(&session)
		if err != nil {
			return nil, err
		}
		sessions = append(sessions, session)
	}

	return sessions, nil
}

func (us *UsersStorage) ValidateSession(token string) (*DBUserSession, error) {
	q := `SELECT id, username, token, valid_from, valid_till
			FROM user_sessions
			WHERE token = $1 AND valid_from <= $2 AND valid_till >= $2`

	now := time.Now().UTC()
	var session DBUserSession

	err := us.DB.Get(&session, q, token, now)
	if err == sql.ErrNoRows {
		return nil, InvalidSession
	} else if err != nil {
		return nil, err
	}

	return &session, nil
}

func (us *UsersStorage) CreateSession(username, token string) (*DBUserSession, error) {
	from := time.Now().UTC()
	session := &DBUserSession{
		Username:  username,
		Token:     token,
		ValidFrom: from,
		ValidTill: from.Add(sessionDuration),
	}

	q := `INSERT INTO user_sessions (username, token, valid_from, valid_till)
			VALUES (:username, :token, :valid_from, :valid_till)`

	_, err := us.DB.NamedExec(q, session)
	if err != nil {
		return nil, err
	}
	return session, nil
}

func NewUsersStorage() (*UsersStorage, error) {
	addr := os.Getenv("DB")
	quit := time.After(10 * time.Second)
	var err error
	var db *sqlx.DB
	for {
		select {
		case <-time.After(2 * time.Second):
			db, err = sqlx.Connect("postgres", addr)
			if err != nil {
				log.Println("Cannot connect to database. Retrying...", err)
				continue
			}
			usersStorage := &UsersStorage{
				DB: db,
			}
			err = usersStorage.initSchema()
			return usersStorage, err
		case <-quit:
			return nil, err
		}
	}
}

func isUniqueError(err error) bool {
	switch err := err.(type) {

	case *pq.Error:
		return err.Code.Class().Name() == "integrity_constraint_violation" && err.Code.Name() == "unique_violation"
	}
	return false
}
