package main

import (
	"crypto/sha512"
	"encoding/hex"
	"encoding/json"
	"log"
	"net/http"

	"github.com/satori/go.uuid"
)

type UsersController struct {
	Storage *UsersStorage
}

func NewUsersController() (controller *UsersController, err error) {
	storage, err := NewUsersStorage()
	if err != nil {
		return
	}
	controller = &UsersController{
		Storage: storage,
	}
	return
}

func (c *UsersController) CreateUser(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	if r.Header.Get("Content-Type") != "application/json" {
		writeResponse(w, http.StatusBadRequest, HttpError{"error", "invalid content type received"})
		return
	}

	var user CreateUserRequest
	err := decoder.Decode(&user)
	if err != nil {
		writeResponse(w, http.StatusBadRequest, HttpError{"error", "invalid data received in request"})
		return
	}

	if len(user.Username) == 0 || len(user.Password) == 0 {
		writeResponse(w, http.StatusBadRequest, HttpError{"error", "username or password cannot be empty"})
		return
	}

	err = c.Storage.CreateUser(user.Username, shaHex(user.Password))
	if err != nil {
		writeResponse(w, http.StatusInternalServerError, HttpError{"error", err.Error()})
		return
	}

	writeResponse(w, http.StatusCreated, nil)
}

func (c *UsersController) LoginUser(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	if r.Header.Get("Content-Type") != "application/json" {
		writeResponse(w, http.StatusBadRequest, HttpError{"error", "invalid content type received"})
		return
	}

	var user LoginRequest
	err := decoder.Decode(&user)
	if err != nil {
		writeResponse(w, http.StatusBadRequest, HttpError{"error", "invalid data received in request"})
		return
	}

	if len(user.Username) == 0 || len(user.Password) == 0 {
		writeResponse(w, http.StatusBadRequest, HttpError{"error", "username or password cannot be empty"})
		return
	}

	dbUser, err := c.Storage.AuthenticateUser(user.Username, shaHex(user.Password))
	if err == UserNotFound {
		writeResponse(w, http.StatusUnauthorized, HttpError{"error", err.Error()})
		return
	} else if err != nil {
		writeResponse(w, http.StatusInternalServerError, HttpError{"error", err.Error()})
		return
	}

	session, err := c.Storage.CreateSession(dbUser.Username, uuid.NewV4().String())
	if err != nil {
		writeResponse(w, http.StatusInternalServerError, HttpError{"error", err.Error()})
		return
	}
	writeResponse(w, http.StatusOK, UserSession{session.Username, session.Token})
}

func (c *UsersController) ListSessions(w http.ResponseWriter, r *http.Request) {
	token := r.Header.Get("Authorization")
	if len(token) == 0 {
		writeResponse(w, http.StatusUnauthorized, HttpError{"error", "session token is not provided"})
		return
	}

	session, err := c.Storage.ValidateSession(token)
	if err != nil {
		if err == InvalidSession {
			writeResponse(w, http.StatusUnauthorized, HttpError{"error", "invalid session token provided"})
		} else {
			writeResponse(w, http.StatusInternalServerError, HttpError{"error", err.Error()})
		}
		return
	}

	sessions, err := c.Storage.ListSessions(session.Username)
	if err != nil {
		writeResponse(w, http.StatusInternalServerError, HttpError{"error", err.Error()})
		return
	}
	writeResponse(w, http.StatusOK, sessions)
}

func writeResponse(w http.ResponseWriter, status int, body interface{}) {
	jsonBody, err := json.Marshal(body)
	if err != nil {
		log.Println("Cannot unmarshal body", err)
		status = http.StatusInternalServerError
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(status)
	if body != nil {
		w.Write(jsonBody)
	}
}

func shaHex(data string) string {
	h := sha512.New()
	h.Write([]byte(data))
	return hex.EncodeToString(h.Sum(nil))
}
