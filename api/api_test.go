package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func createController(t *testing.T) *UsersController {
	controller, err := NewUsersController()
	if err != nil {
		t.Fatal("Cannot create controller")
	}

	_, err = controller.Storage.DB.Exec("TRUNCATE users CASCADE")
	if err != nil {
		t.Errorf("Cannot truncate users %s", err)
	}

	_, err = controller.Storage.DB.Exec("TRUNCATE user_sessions")
	if err != nil {
		t.Errorf("Cannot truncate user sessions")
	}
	return controller
}

func TestRegisterUser(t *testing.T) {
	controller := createController(t)
	user := CreateUserRequest{Username: "user", Password: "password"}
	body, err := json.Marshal(user)
	if err != nil {
		t.Errorf("User should be json marshable")
	}

	req, err := http.NewRequest("POST", "/user", bytes.NewBuffer(body))
	if err != nil {
		t.Errorf("Cannot create request")
	}
	req.Header.Set("Content-Type", "application/json")

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(controller.CreateUser)
	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusCreated {
		t.Errorf("Expected %d, got %d", http.StatusCreated, rr.Code)
	}
}

func TestRegisterUserAlreadyExists(t *testing.T) {
	controller := createController(t)
	user := CreateUserRequest{Username: "user", Password: "password"}
	body, err := json.Marshal(user)
	if err != nil {
		t.Errorf("User should be json marshable")
	}

	req, err := http.NewRequest("POST", "/user", bytes.NewBuffer(body))
	if err != nil {
		t.Errorf("Cannot create request")
	}
	req.Header.Set("Content-Type", "application/json")

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(controller.CreateUser)
	handler.ServeHTTP(rr, req)

	rr = httptest.NewRecorder()
	req, err = http.NewRequest("POST", "/user", bytes.NewBuffer(body))
	req.Header.Set("Content-Type", "application/json")
	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusInternalServerError {
		t.Errorf("Expected %d, got %d", http.StatusInternalServerError, rr.Code)
	}
}

func TestUserLogin(t *testing.T) {
	controller := createController(t)
	user := CreateUserRequest{Username: "user", Password: "password"}
	body, err := json.Marshal(user)
	if err != nil {
		t.Errorf("User should be json marshable")
	}

	req, err := http.NewRequest("POST", "/user", bytes.NewBuffer(body))
	if err != nil {
		t.Errorf("Cannot create request")
	}
	req.Header.Set("Content-Type", "application/json")

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(controller.CreateUser)
	handler.ServeHTTP(rr, req)

	loginRequest := LoginRequest{Username: "user", Password: "password"}
	body, err = json.Marshal(loginRequest)
	req, err = http.NewRequest("POST", "/user/login", bytes.NewBuffer(body))
	if err != nil {
		t.Errorf("Cannot create request")
	}
	req.Header.Set("Content-Type", "application/json")

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(controller.LoginUser)
	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusOK {
		t.Errorf("Expected %d, got %d", http.StatusOK, rr.Code)
	}
}

func TestInvalidUserLogin(t *testing.T) {
	controller := createController(t)

	loginRequest := LoginRequest{Username: "user", Password: "password"}
	body, err := json.Marshal(loginRequest)
	req, err := http.NewRequest("POST", "/user/login", bytes.NewBuffer(body))
	if err != nil {
		t.Errorf("Cannot create request")
	}
	req.Header.Set("Content-Type", "application/json")

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(controller.LoginUser)
	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusUnauthorized {
		t.Errorf("Expected %d, got %d", http.StatusUnauthorized, rr.Code)
	}
}

func TestListSessions(t *testing.T) {
	controller := createController(t)

	user := CreateUserRequest{Username: "user", Password: "password"}
	body, err := json.Marshal(user)
	if err != nil {
		t.Errorf("User should be json marshable")
	}

	req, err := http.NewRequest("POST", "/user", bytes.NewBuffer(body))
	if err != nil {
		t.Errorf("Cannot create request")
	}
	req.Header.Set("Content-Type", "application/json")

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(controller.CreateUser)
	handler.ServeHTTP(rr, req)

	loginRequest := LoginRequest{Username: "user", Password: "password"}
	body, err = json.Marshal(loginRequest)
	req, err = http.NewRequest("POST", "/user/login", bytes.NewBuffer(body))
	if err != nil {
		t.Errorf("Cannot create request")
	}
	req.Header.Set("Content-Type", "application/json")

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(controller.LoginUser)
	handler.ServeHTTP(rr, req)

	var session UserSession
	decoder := json.NewDecoder(rr.Body)
	decoder.Decode(&session)

	req, err = http.NewRequest("GET", "/user/session", bytes.NewBuffer(body))
	if err != nil {
		t.Errorf("Cannot create request")
	}
	req.Header.Set("Authorization", session.Token)

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(controller.ListSessions)
	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusOK {
		t.Errorf("Expected %d, got %d", http.StatusOK, rr.Code)
	}

	var sessions []DBUserSession
	decoder = json.NewDecoder(rr.Body)
	decoder.Decode(&sessions)

	if len(sessions) != 1 {
		t.Errorf("Expected %d, got %d", 1, len(sessions))
	}
}
