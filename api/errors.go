package main

import "errors"

var UserAlreadyExistsError = errors.New("user already exists")
var UserNotFound = errors.New("invalid username or password")
var InvalidSession = errors.New("invalid session")
