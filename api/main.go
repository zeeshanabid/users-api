package main

import (
	"log"
	"net/http"

	"github.com/go-zoo/bone"
)

func main() {
	mux := bone.New()

	controller, err := NewUsersController()
	if err != nil {
		panic(err)
	}

	mux.PostFunc("/user", controller.CreateUser)
	mux.PostFunc("/user/login", controller.LoginUser)
	mux.GetFunc("/user/session", controller.ListSessions)

	addr := ":8080"
	log.Println("Starting API on address", addr)
	err = http.ListenAndServe(addr, mux)
	if err != nil {
		panic(err)
	}
}
